/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.upgrade;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.jersey.api.client.Client;

import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.rest.AccessTokenDTO;
import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.common.mdrclient.MdrClient;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.Database;
import de.samply.edc.osse.model.CaseForm;
import de.samply.edc.osse.model.EpisodeForm;
import de.samply.edc.osse.model.Form;
import de.samply.edc.osse.model.UpgradeDBModel;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.osse.utils.FormImportHelper;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;

/**
 * This upgrade changes the saving strategy for non-repeatable records Those
 * were not yet saved with referece to their record, but were only used as
 * "display-frame" in the form itself. Now those will be saved like repeatables,
 * as if they are a datatable with one entry
 *
 * This upgrade also puts the system role into the system
 */
public class UpgradeExecution5 extends UpgradeExecution {

    /** The fih. */
    private FormImportHelper fih = new FormImportHelper(applicationBean.getConfig());
    
    /** The access token. */
    private AccessTokenDTO accessToken;
    
    /** The mdr jersey client. */
    private Client frJerseyClient, mdrJerseyClient;
    
    /** The form storage. */
    private JSONResource formStorage = new JSONResource();

    /**
     * Instantiates a new upgrade execution3.
     *
     * @param upgradeData            the upgrade data
     * @param currentOSSEConfig            the current osse config
     * @param applicationBean the application bean
     */
    public UpgradeExecution5(Upgrade upgradeData, JSONResource currentOSSEConfig, ApplicationBean applicationBean) {
        super(upgradeData, currentOSSEConfig, applicationBean);

        fromVersion = new VersionNumber("1.1.1");
        toVersion = new VersionNumber("1.1.2");
    }

    /**
     * Instantiates a new upgrade execution3.
     *
     * @param currentOSSEConfig            the current osse config
     * @param applicationBean the application bean
     */
    public UpgradeExecution5(JSONResource currentOSSEConfig, ApplicationBean applicationBean) {
        super(currentOSSEConfig, applicationBean);

        fromVersion = new VersionNumber("1.1.1");
        toVersion = new VersionNumber("1.1.2");
    }

    /**
     * Do post upgrade.
     *
     * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPostUpgrade()
     */
    @Override
    public void doPostUpgrade() {
    }

    /**
     * Do pre upgrade.
     *
     * @return the boolean
     * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPreUpgrade()
     */
    @Override
    public Boolean doPreUpgrade() {
        Database database = new Database(true);
        try {
            // We call this to verify the system role was selectable and is
            // valid.
            // If not, a null pointer exception is thrown
            database.getDatabaseModel();
        } catch (Exception e) {
            // so we insert it now
            Utils.getLogger().debug("Inserting System role now");
            insertSystemRole();
        }

        try {
            return changeRecordSavingStrategy();
        } catch (DatabaseException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Inserts the system role into the system. If the role already exists, none
     * will be imported, the error will be ignored
     */
    private void insertSystemRole() {
        try {
            UpgradeDBModel tmodel = new UpgradeDBModel(databaseConfigFile);
            tmodel.insertSystemRole();
            tmodel.logout();
            tmodel.close();
        } catch (DatabaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Recreates a certain form, old versions will be deleted and replaced.
     *
     * @param formName            the name of the form
     * @return true, if successful
     * @throws FileNotFoundException the file not found exception
     */
    private boolean doRecover(String formName) throws FileNotFoundException {
        String saveFileName = "/forms/" + formName + ".xhtml";
        String saveFileNamePure = saveFileName;
        saveFileName = Utils.getRealPath(saveFileName);
        File file = new File(saveFileName);

        if (formStorage.getProperty(saveFileNamePure) != null) {
            Utils.getLogger().debug("This form was stored, removing it.");
            formStorage.removeProperties(saveFileNamePure);
        }

        if (file.exists()) {
            Utils.getLogger().debug(
                    "Form " + formName + " exists as form in " + saveFileName
                            + ". Deleting it.");
            file.delete();
        }

        Pattern pattern = Pattern.compile("^form_([^-]+)_ver-([^-]+)$");
        Matcher matcher = pattern.matcher(formName);
        if (!matcher.find()) {
            Utils.getLogger().debug("Formname " + formName + " does not match pattern.");
            return false;
        }

        String mdrURL = (String) fih.getConfiguration().getProperty(Vocabulary.Config.MDR.REST);
        MdrClient mdrClient = new MdrClient(mdrURL, mdrJerseyClient);

        formStorage = fih.doImport(frJerseyClient, mdrClient,
                accessToken, matcher.group(1), matcher.group(2),
                false, formStorage, true, false);

        return true;
    }

    /**
     * Reworks all forms and updates already saved data to the new
     * non-repeatable saving strategy.
     *
     * @return boolean
     * @throws DatabaseException the database exception
     */
    private Boolean changeRecordSavingStrategy() throws DatabaseException {
        Database database = new Database(true);
        try {
            // We call this to verify the system role was selectable and is
            // valid.
            // If not, a null pointer exception is thrown
            database.getDatabaseModel();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        JSONResource osseFormStorage = applicationBean.getOSSEFormStorage();

        if (osseFormStorage.getProperty(Vocabulary.Config.Form.storage) != null) {
            formStorage = osseFormStorage.getProperty(Vocabulary.Config.Form.storage).asJSONResource();
        }

        if (osseFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix) != null) {
            fih.setFormNameMatrix(osseFormStorage.getProperty(
                    Vocabulary.Config.Form.formNameMatrix).asJSONResource());
        }

        HttpConnector43 httpConnector = null;
        Client client = null;

        try {
            httpConnector = new HttpConnector43(applicationBean.getConfig());
            // Get a HTTPS jersey client (AUTH is always https)
            client = httpConnector.getClient(httpConnector
                    .getHttpClientForHTTPS());
            accessToken = Auth.getAccessToken(client, applicationBean.getConfig());
            client.destroy();
        } catch (HttpConnectorException e1) {
            e1.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (JWTException e) {
            e.printStackTrace();
        }

        if (httpConnector == null) {
            Utils.getLogger().error("HTTPclient could not be started");
            Utils.addContextMessage("Failed",
                    "A critical error occured (HTTPclient could not be started.");
            return false;
        }

        // check if the registry was already registered in AUTH
        if (!applicationBean.doWeHaveAuthUserId()) {
            Utils.getLogger().error("AUTH User ID yet not created. This does not need to continue.");
            return true;
        }

        if (accessToken == null) {
            Utils.getLogger().error("No access token could be gained");
            Utils.addContextMessage("Failed",
                    "A critical error occured (No access token was returned.");
            return false;
        }

        // Get a Jersey client for the FormRepository
        String urlFR = applicationBean.getConfig()
                .getProperty(Vocabulary.Config.FormEditor.formEditorBASE) + "";
        frJerseyClient = httpConnector.getJerseyClient(urlFR);

        // Get a Jersey client for the MDR
        String urlMDR = applicationBean.getConfig()
                .getProperty(Vocabulary.Config.MDR.REST) + "";
        mdrJerseyClient = httpConnector.getJerseyClient(urlMDR);

        // 1) rework all forms
        try {
            if (applicationBean.getArchivedFormulars() != null) {
                for (String formName : applicationBean.getArchivedFormulars().get("en").keySet()) {
                    if (!doRecover(formName))
                        return false;
                }
            }

            if (applicationBean.getFormulars() != null) {
                for (String formName : applicationBean.getFormulars().get("en").keySet()) {
                    if (!doRecover(formName))
                        return false;
                }
            }

            if (applicationBean.getArchivedVisitFormulars() != null) {
                for (String formName : applicationBean.getArchivedVisitFormulars().get("en").keySet()) {
                    if (!doRecover(formName))
                        return false;
                }
            }

            if (applicationBean.getVisitFormulars() != null) {
                for (String formName : applicationBean.getVisitFormulars().get("en").keySet()) {
                    if (!doRecover(formName))
                        return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        osseFormStorage.setProperty(Vocabulary.Config.Form.storage, formStorage);

        database.saveConfig("osse.form.storage", osseFormStorage);

        HashMap<String, List<String>> formAndItsRecords = new HashMap<>();

        currentOSSEConfig.setProperty(Vocabulary.Config.mdrEntityIsInForm, fih.getMdrKeyUsageStore().asJSONResource());

        JSONResource recordHasMdrEntities = new JSONResource();
        for (String key : fih.getRecordHasMdrEntities().keySet()) {
            LinkedHashSet<String> moo = fih.getRecordHasMdrEntities().get(key);
            for (String me : moo)
                recordHasMdrEntities.addProperty(key, me);
        }
        currentOSSEConfig.setProperty(Vocabulary.Config.recordHasMdrEntities, recordHasMdrEntities);

        Utils.getLogger().debug("Forms redone.");

        // Now change the already saved values to new strategy
        // Run through the forms that have records
        for (String formName : formAndItsRecords.keySet()) {
            Utils.getLogger().debug("Running form: " + formName);

            List<String> records = formAndItsRecords.get(formName);

            Boolean isCaseForm = applicationBean.isUniqueForm(formName);
            Boolean isEpisodeForm = applicationBean.isEpisodeForm(formName);

            if (isCaseForm) {
                Utils.getLogger().debug("It's a case form");

                ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.CaseForm);
                query.add(Criteria.Equal(OSSEVocabulary.Type.CaseForm, OSSEVocabulary.CaseForm.Name, formName));
                // List of all forms saved with the given name
                ArrayList<Resource> found = database.getResources(query);
                Utils.getLogger().debug("The list of caseforms with that name size: " + found.size());

                for (Resource formRes : found) {
                    Form formEntity = new CaseForm(database, formRes);
                    doChangeSave(formEntity, records);
                }
            } else if (isEpisodeForm) {
                Utils.getLogger().debug("It's an episode form");

                ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.EpisodeForm);
                query.add(Criteria.Equal(OSSEVocabulary.Type.EpisodeForm, OSSEVocabulary.EpisodeForm.Name, formName));
                // List of all forms saved with the given name
                ArrayList<Resource> found = database.getResources(query);
                Utils.getLogger().debug("The list of episodeforms with that name size: " + found.size());

                for (Resource formRes : found) {
                    Form formEntity = new EpisodeForm(database, formRes);
                    doChangeSave(formEntity, records);
                }

            }

        }

        return true;
    }

    /**
     * Do change save.
     *
     * @param formEntity the form entity
     * @param records the records
     */
    private void doChangeSave(Form formEntity, List<String> records) {
        formEntity.load();

        Boolean didChange = false;

        // Get all records of this form
        for (String recordMdrId : records) {
            Utils.getLogger().debug("Running recordid " + recordMdrId);
            String loadRecordMDRId = Utils.fixMDRkeyForLoad(recordMdrId);
            // Get all members
            LinkedHashSet<String> members = fih.getRecordHasMdrEntities().get(recordMdrId);
            for (String mdrId : members) {
                String loadMDRId = Utils.fixMDRkeyForLoad(mdrId);
                Utils.getLogger().debug(
                        "Running member " + mdrId + " it has the value:" + formEntity.getProperty(loadMDRId));
                // check if a value if that mdrID exists
                if (formEntity.getProperty(loadMDRId) != null) {
                    // save it in the new strategy as well
                    String combinedMDRId = loadRecordMDRId + "/" + loadMDRId;
                    formEntity.addProperty(combinedMDRId, formEntity.getProperty(loadMDRId));

                    Utils.getLogger().debug(
                            "Has a value " + formEntity.getProperty(loadMDRId) + " saving it under " + combinedMDRId);
                    didChange = true;
                }
            }
        }

        if (didChange) {
            Utils.getLogger().debug("Saving form");
            Entity lastChangedBy = (Entity) formEntity.getProperty(Vocabulary.attributes.lastChangedBy);
            formEntity.setProperty(Vocabulary.attributes.lastChangedBy, lastChangedBy.getResource());

            formEntity.getDatabase().beginTransaction();
            formEntity.saveOrUpdate();
            formEntity.getDatabase().commit();
        } else {
            Utils.getLogger().debug("As nothing was changed, nothing to save.");
        }

    }
}
