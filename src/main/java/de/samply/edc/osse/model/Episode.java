/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.converter.DataConverter;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.catalog.EpisodePatterns;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.TimestampLiteral;
import de.samply.store.osse.OSSEVocabulary;

/**
 * Model class for episodes (formerly called visit).
 */
public class Episode extends Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The amount of reported forms. */
    private Integer amountReportedForms = 0;

    /** The amount of validated forms. */
    private Integer amountValidatedForms = 0;

    /**  The list of reported forms. */
    private ArrayList<Form> formsWithReports = null;

    /**
     * Instantiates a new episode.
     *
     * @param database
     *            the database
     */
    public Episode(AbstractDatabase<?> database) {
        super(database, OSSEVocabulary.Type.Episode);
        setChildrenHashPrimaryKey(OSSEVocabulary.Type.EpisodeForm,
                OSSEVocabulary.EpisodeForm.Name);
    }

    /**
     * Instantiates a new episode.
     *
     * @param database
     *            the database
     * @param episodeRes
     *            the resource of the episode
     */
    public Episode(AbstractDatabase<?> database, Resource episodeRes) {
        super(database, OSSEVocabulary.Type.Episode, episodeRes);
        setChildrenHashPrimaryKey(OSSEVocabulary.Type.EpisodeForm,
                OSSEVocabulary.EpisodeForm.Name);
    }

    /**
     * Instantiates a new episode.
     *
     * @param database
     *            the database
     * @param episodeURI
     *            the backend URI of the episode
     */
    public Episode(AbstractDatabase<?> database, String episodeURI) {
        super(database, OSSEVocabulary.Type.Episode, episodeURI);
        setChildrenHashPrimaryKey(OSSEVocabulary.Type.EpisodeForm,
                OSSEVocabulary.EpisodeForm.Name);
    }

    /**
     * Creates a new episode or loads an existing one by name
     *
     * @param name the name
     * @param myCase the my case
     */
    public void createOrLoadByName(String name, Case myCase) {
        Resource episodeResource = Entity.entityExistsByPropertyAsChildOfParentID(getDatabase(), getType(),
                OSSEVocabulary.Episode.Name, name,
                OSSEVocabulary.Type.Case, myCase.getId());

        if (episodeResource == null) {
            setProperty(OSSEVocabulary.Location.Name, name);
            setParent(myCase);
            setProperty(OSSEVocabulary.Episode.Case, getParent(OSSEVocabulary.Type.Case).getResource());
            Date date = Episode.getDateByPattern(name, null);
            setProperty("timestamp", new TimestampLiteral(date.getTime()));

            saveOrUpdate();
        } else {
            setResource(episodeResource);
        }
        load();
    }

    /**
     * Saves the episode.
     *
     * @return the boolean
     * @see de.samply.edc.model.Entity#saveOrUpdate()
     */
    @Override
    public Boolean saveOrUpdate() {
        // TODO: add this, check first where the case was added in another way
        // than via setParent
        // addProperty(OSSEVocabulary.Episode.Case,
        // getParent(OSSEVocabulary.Type.Case).getResource());

        return super.saveOrUpdate();
    }

    /**
     * Deletes an episode. This will mark an episode as deleted, and also rename
     * it, so the name can be used for another (new) episode again.
     *
     * @return the boolean
     * @see de.samply.edc.model.Entity#delete()
     */
    @Override
    public Boolean delete() {
        super.delete();
        Resource visitResource = getResource();

        visitResource.setProperty(
                OSSEVocabulary.Episode.Name,
                getProperty(OSSEVocabulary.Episode.Name) + "_deleted_"
                        + Utils.generateRandomString(10, false));
        visitResource.removeProperties("timestamp");
        getDatabase().save(visitResource);

        return true;
    }

    /**
     * Recognizes the pattern by which the episode was named.
     *
     * @param value            the name to check
     * @return the found EpisodePattern
     */
    public static EpisodePattern getEpisodePattern(String value) {
        if (value == null)
            return null;

        for (String name : EpisodePatterns.patterns.keySet()) {
            Pattern pattern = Pattern.compile(EpisodePatterns.patterns.get(name).getRegexp());
            Matcher matcher = pattern.matcher(value);
            if (!matcher.find()) {
                continue;
            }

            return EpisodePatterns.patterns.get(name);
        }

        return null;
    }

    /**
     * Returns a string representation of a date formatted by a given pattern.
     *
     * @param value            the date
     * @param patternName            the format pattern name
     * @return the string representation
     */
    public static String getStringByPattern(Date value, String patternName) {
        for (String name : EpisodePatterns.patterns.keySet()) {
            if (patternName != null && !patternName.equalsIgnoreCase(name))
                continue;

            if (!EpisodePatterns.patterns.get(name).getIsDatePattern()) {
                String year = DataConverter.convertToDateString(value, "yyyy", "UTC");
                Integer month = Integer.parseInt(DataConverter.convertToDateString(value, "MM", "UTC"));
                String quarter = "";

                Boolean romanQuarter = name.equalsIgnoreCase("ROMANQUARTERS");

                switch (month) {
                case 1:
                case 2:
                case 3:
                    quarter = (romanQuarter ? "I" : "Q1");
                    break;
                case 4:
                case 5:
                case 6:
                    quarter = (romanQuarter ? "II" : "Q2");
                    break;
                case 7:
                case 8:
                case 9:
                    quarter = (romanQuarter ? "III" : "Q3");
                    break;
                default:
                    quarter = (romanQuarter ? "IV" : "Q4");
                    break;
                }

                return quarter + "/" + year;
            }

            String dateString = DataConverter.convertToDateString(value, EpisodePatterns.patterns.get(name)
                    .getPattern(), "UTC");
            if (dateString != null)
                return dateString;
        }

        return null;
    }

    /**
     * Method to convert a string into a Date class by any episode pattern
     * possible.
     *
     * @param value            The string to convert
     * @param patternName            the pattern to use, or any possible if null
     * @return Date The date
     */
    public static Date getDateByPattern(String value, String patternName) {
        for (String name : EpisodePatterns.patterns.keySet()) {
            if (patternName != null && !patternName.equalsIgnoreCase(name))
                continue;

            Boolean romanQuarter = name.equalsIgnoreCase("ROMANQUARTERS");
            Boolean normalQuarter = name.equalsIgnoreCase("QUARTERS");

            if (romanQuarter || normalQuarter) {
                // grab the quarter pattern
                Pattern pattern = Pattern.compile(EpisodePatterns.patterns.get(name).getRegexp());
                Matcher matcher = pattern.matcher(value);
                if (!matcher.find()) {
                    continue;
                }

                String dateString = matcher.group(2) + "-";
                name = "ISODATE";
                if ((normalQuarter && matcher.group(1).equalsIgnoreCase("Q1"))
                        || (romanQuarter && matcher.group(1).equalsIgnoreCase("I"))) {
                    dateString += "01-01";
                } else if ((normalQuarter && matcher.group(1).equalsIgnoreCase("Q2"))
                        || (romanQuarter && matcher.group(1).equalsIgnoreCase("II"))) {
                    dateString += "04-01";
                } else if ((normalQuarter && matcher.group(1).equalsIgnoreCase("Q3"))
                        || (romanQuarter && matcher.group(1).equalsIgnoreCase("III"))) {
                    dateString += "07-01";
                } else {
                    dateString += "10-01";
                }
                value = dateString;
            }

            Pattern pattern = Pattern.compile(EpisodePatterns.patterns.get(name).getRegexp());
            Matcher matcher = pattern.matcher(value);
            if (!matcher.find()) {
                continue;
            }

            Date theDate = DataConverter.convertFromDateString(value, EpisodePatterns.patterns.get(name).getPattern(),
                    "UTC");
            if (theDate != null) {
                // for sorting purposes we add 1ms to monthlies and 2ms to
                // normal dates
                if (romanQuarter || normalQuarter) {
                    // not adding anything to quarters
                } else if (name.equals("MONTHYEAR")) {
                    theDate.setTime(theDate.getTime() + 1);
                } else if (EpisodePatterns.patterns.get(name).getIsDatePattern()) {
                    theDate.setTime(theDate.getTime() + 2);
                }
                return theDate;
            }
        }

        return null;
    }

    /**
     * Compares an episode with this episode based on their represented date In
     * order to make Quarters, Monthlies and normal dates order correctly, we
     * add 1s to monthlies and 2s to normal dates.
     *
     * @param otherEpisode the other episode
     * @return the int
     * @see de.samply.edc.model.Entity#compareTo(de.samply.edc.model.Entity)
     */
    @Override
    public int compareTo(Entity otherEpisode) {
        if (otherEpisode == null) {
            return -1;
        }

        EpisodePattern myPattern = Episode.getEpisodePattern(getName());
        EpisodePattern hisPattern = Episode.getEpisodePattern(((Episode) otherEpisode).getName());

        Date myDate = Episode.getDateByPattern(getName(), myPattern.getName());
        Date hisDate = Episode.getDateByPattern(((Episode) otherEpisode).getName(), hisPattern.getName());

        // compare dates
        return myDate.compareTo(hisDate);
    }

    /**
     * Adds a form to the episode and also increases the counter for validated
     * or reported forms.
     *
     * @param type the type
     * @param form the form
     * @see de.samply.edc.model.Entity#addChild(java.lang.String,
     *      de.samply.edc.model.Entity)
     */
    @Override
    public void addChild(String type, Entity form) {
        if (form == null)
            return;

        if (!(form instanceof EpisodeForm))
            return;

        if (((EpisodeForm) form).isValidated()) {
            amountValidatedForms++;
        } else if (((EpisodeForm) form).isReported()) {
            amountReportedForms++;
        }

        super.addChild(type, form);
    }

    /**
     * Removes a from from the episode and also decreases the counter for
     * validated or reported forms.
     *
     * @param type the type
     * @param form the form
     * @see de.samply.edc.model.Entity#removeChild(java.lang.String,
     *      de.samply.edc.model.Entity)
     */
    @Override
    public void removeChild(String type, Entity form) {
        if (form == null)
            return;

        if (!(form instanceof EpisodeForm))
            return;

        if (!hasChild(type, form)) {
            return;
        }
        if (((EpisodeForm) form).isValidated()) {
            amountValidatedForms--;
        } else if (((EpisodeForm) form).isReported()) {
            amountReportedForms--;
        }

        super.removeChild(type, form);
    }

    /**
     * Loads the forms of the episode.
     *
     * @see de.samply.edc.model.Entity#loadChildren(java.lang.String)
     */
    public void loadEpisodeForms() {
        amountValidatedForms = 0;
        amountReportedForms = 0;

        super.loadChildren(OSSEVocabulary.Type.EpisodeForm, false);

        // get the number of reported and validated children
        for (Entity form : getChildren(OSSEVocabulary.Type.EpisodeForm)) {
            if (((EpisodeForm) form).isValidated()) {
                amountValidatedForms++;
            } else if (((EpisodeForm) form).isReported()) {
                amountReportedForms++;
            }
        }
    }

    /**
     * All forms status text.
     *
     * @return the string
     */
    public String allFormsStatusText() {
        if (allFormsAreValidated())
            return "validated";

        if (allFormsAreReported())
            return "reported";

        return "";
    }

    /**
     * Checks if all forms in an episode are marked as reported or validated.
     *
     * @return the boolean
     */
    public Boolean allFormsAreReported() {
        Integer amountEpisodeForms = ((ApplicationBean) Utils.getAB())
                .getAmountEpisodeFormulars();

        if ((amountReportedForms + amountValidatedForms) == amountEpisodeForms)
            return true;
        else
            return false;
    }

    /**
     * Checks if all forms in an episode are marked as validated.
     *
     * @return the boolean
     */
    public Boolean allFormsAreValidated() {
        Integer amountVisitForms = ((ApplicationBean) Utils.getAB())
                .getAmountEpisodeFormulars();

        if (amountValidatedForms.equals(amountVisitForms))
            return true;
        else
            return false;
    }

    /**
     * Gets a form of the episode by its name.
     *
     * @param formname
     *            the name of the form
     * @return the form
     */
    public Form getForm(String formname) {
        Entity found = getChild(OSSEVocabulary.Type.EpisodeForm, formname);
        if (found != null)
            return (EpisodeForm) found;
        else
            return null;
    }

    /**
     * Sets the amount of reported forms.
     *
     * @param amountReportedForms
     *            the new amount reported forms
     */
    public void setAmountReportedForms(Integer amountReportedForms) {
        this.amountReportedForms = amountReportedForms;
    }

    /**
     * Sets the amount of validated forms.
     *
     * @param amountValidatedForms
     *            the new amount validated forms
     */
    public void setAmountValidatedForms(Integer amountValidatedForms) {
        this.amountValidatedForms = amountValidatedForms;
    }

    /**
     * Gets the amount of validated forms.
     *
     * @return the amount validated forms
     */
    public Integer getAmountValidatedForms() {
        return amountValidatedForms;
    }

    /**
     * Gets the amount of reported forms.
     *
     * @return the amount reported forms
     */
    public Integer getAmountReportedForms() {
        return amountReportedForms;
    }

    /**
     * Gets the name of this episode.
     *
     * @return the name
     */
    public String getName() {
        return (String) getProperty(OSSEVocabulary.Episode.Name);
    }

    /**
     * Gets the visit label (now called name).
     *
     * @return the visit label
     */
    @Deprecated
    public String getVisitLabel() {
        return (String) getProperty(OSSEVocabulary.Episode.Name);
    }

    /**
     * Gets the optional text.
     *
     * @return the optional text
     */
    public String getOptionalText() {
        return (String) getProperty(Vocabulary.Episode.optionalText);
    }

    /**
     * Gets the forms with reports.
     *
     * @return the forms with reports
     */
    public ArrayList<Form> getFormsWithReports() {
        return formsWithReports;
    }

    /**
     * Sets the forms with reports.
     *
     * @param formsWithReports
     *            the new forms with reports
     */
    public void setFormsWithReports(ArrayList<Form> formsWithReports) {
        this.formsWithReports = formsWithReports;
    }
}
