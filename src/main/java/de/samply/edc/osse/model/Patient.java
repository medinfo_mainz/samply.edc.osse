/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.pseudonymisierung.mainzelliste.client.ID;
import de.pseudonymisierung.mainzelliste.client.InvalidSessionException;
import de.pseudonymisierung.mainzelliste.client.MainzellisteNetworkException;
import de.pseudonymisierung.mainzelliste.client.Session;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.Database;
import de.samply.edc.osse.control.SessionBean;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSEVocabulary;

/**
 * Model class for patients.
 */
public class Patient extends Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**  The TempID for this patient by the Mainzelliste. */
    private String mainzellisteTempID;

    /**  Show the IDAT to the user or not?. */
    private Boolean showIdat;

    /** The location of the patient. */
    private Location location;

    /**  define if this patient has a mainzelliste pseudonym or not. */
    private Boolean noMainzelliste = false;

    /**  The user that belongs to this patient. */
    private User asUser;

    /**
     * Instantiates a new patient.
     *
     * @param database
     *            the database
     */
    public Patient(AbstractDatabase<?> database) {
        super(database, OSSEVocabulary.Type.Patient);
    }

    /**
     * Instantiates Patient based on Resource.
     *
     * @param database
     *            the database
     * @param patientResource
     *            the patient resource
     */
    public Patient(AbstractDatabase<?> database, Resource patientResource) {
        super(database, OSSEVocabulary.Type.Patient, patientResource);
    }

    /**
     * Instantiates Patient based on Resource.
     *
     * @param database
     *            the database
     * @param patientURI
     *            the patient backend URI
     */
    public Patient(AbstractDatabase<?> database, String patientURI) {
        super(database, OSSEVocabulary.Type.Patient, patientURI);
    }

    /**
     * Imports a patient, used in Bridgehead and Importer If a patient is newly
     * created, also a case is created for the given location.
     *
     * @param patientID            A pseudonym
     * @param location            Initial location of the patient
     * @param overwrite            If the patient shall be overwritten if he already exists
     * @return -1: removed old patient, 0: loaded existing patient, 1: added new
     *         patient
     */
    public Integer importPatient(String patientID, Location location, Boolean overwrite) {
        noMainzelliste = true;

        Integer returnCode = 1;
        Resource patientResource = entityExistsByProperty("patientID", patientID);

        if (patientResource != null) {
            setResource(patientResource);

            if (overwrite) {
                Utils.getLogger().debug("-> Deleting patient from DB");
                // exists, but we want to overwrite him
                wipeoutPatient();
                returnCode = -1;
            } else {
                Utils.getLogger().debug("-> Patient already exists. Loading data.");
                load();
                loadChildren(OSSEVocabulary.Type.Case, true);
                return 0;
            }
        }

        // create patient
        setProperty(OSSEVocabulary.Patient.Locations, location.getResource());
        setProperty("patientID", patientID);
        setProperty(Vocabulary.Patient.isImported, true);

        saveOrUpdate();
        Case myCase = new Case(getDatabase());
        myCase.setParent(this);
        myCase.setLocation(location);
        myCase.saveOrUpdate();

        addChild(OSSEVocabulary.Type.Case, myCase);

        return returnCode;
    }

    /**
     * Adds a case to a certain location.
     *
     * @param location the location
     * @return the case
     */
    public Case addCaseOfLocation(Location location) {
        getDatabase().beginTransaction();
        Case myCase = new Case(getDatabase());
        myCase.setParent(this);
        myCase.setLocation(location);
        myCase.saveOrUpdate();
        getDatabase().commit();
        addChild(OSSEVocabulary.Type.Case, myCase);

        return myCase;
    }

    /**
     * Gets the case of a location.
     *
     * @param location the location
     * @return the case of location
     */
    public Entity getCaseOfLocation(Location location) {
        if (!hasCases())
            return null;

        if (location == null)
            return null;

        for (Entity aCase : getCases()) {
            if (((Case) aCase).getLocation().getName().equalsIgnoreCase(location.getName()))
                return aCase;
        }

        return null;
    }

    /**
     * Gets the cases of this patient.
     *
     * @return the cases
     */
    public List<Entity> getCases() {
        return getSortedChildren(OSSEVocabulary.Type.Case);
    }

    /**
     * Gets the case names of this patient (not yet used).
     *
     * @return the cases names
     */
    public List<String> getCasesNames() {
        List<String> ret = new ArrayList<String>();
        for (Entity theCase : getCases()) {
            ret.add((String) theCase.getProperty("name"));
        }
        return ret;
    }

    /**
     * Checks if the patient exists in a location checks the following: 1)
     * patient exists at all 2) patient's location 3) patient's cases locations.
     *
     * @param patientID            The patientID
     * @param locationName            The location name
     * @return boolean
     */
    public Boolean existsInLocation(String patientID, String locationName) {
        // grab the patient with said ID
        Resource thisPatient = entityExistsByProperty("patientID", patientID);
        if (thisPatient == null)
            return false;

        Patient test = new Patient(getDatabase(), thisPatient);
        test.load();

        // does the patient's location already fit?
        if (test.getLocation().getName().equalsIgnoreCase(locationName))
            return true;

        // check the cases of the patient
        for (Entity aCase : test.getCases()) {
            if (((Case) aCase).getLocation().getName().equalsIgnoreCase(locationName))
                return true;
        }

        return false;
    }

    /**
     * Wipes out a patient from the DB completely.
     */
    public void wipeoutPatient() {
        if (getResource() == null)
            return;

        ((Database) getDatabase()).wipeoutPatient(getResource());
        setResource(null);
    }

    /**
     * Deletes this patient and his cases.
     *
     * @return the boolean
     */
    public Boolean deletePatient() {
        deleteChildren(OSSEVocabulary.Type.Case);

        delete();

        storeLastChange();

        return true;
    }

    /**
     * deletes a case from this patient.
     *
     * @param deleteThisCase the delete this case
     * @return the boolean
     */
    public Boolean deleteCaseOfPatient(Case deleteThisCase) {
        if (!deleteThisCase.delete())
            return false;

        storeLastChange();
        return true;
    }

    /**
     * undeletes a case of this patient.
     *
     * @param unDeleteThisCase the un delete this case
     * @return the boolean
     */
    public Boolean unDeleteCaseOfPatient(Case unDeleteThisCase) {
        if (!unDeleteThisCase.undelete())
            return false;

        storeLastChange();
        return true;
    }

    /**
     * Undelete this patient (but not his cases!).
     *
     * @return the boolean
     */
    public Boolean undeletePatient() {

        undelete();

        storeLastChange();

        return true;
    }

    /**
     * Gets the Mainzelliste TempID of the patient.
     *
     * @return the Mainzelliste TempID
     */
    public String getPseudonym() {
        return mainzellisteTempID;
    }

    /**
     * Gets the backend permissions active on this patient.
     *
     * @return the active permissions
     */
    public ArrayList<Value> getActivePermissions() {
        if (getResource() != null
                && getResource().getProperty(OSSEOntology.ActivePermissions) != null)
            return getResource().getProperties(OSSEOntology.ActivePermissions);

        return new ArrayList<Value>();
    }

    /**
     * Load the basic data of the patient.
     *
     * @return the boolean
     */
    @Override
    public Boolean load() {
        super.load();

        // determine if IDAT may be displayed to the current user or not
        showIdat = false;

        for (Value aPermission : getActivePermissions()) {

            if (aPermission != null && aPermission instanceof JSONResource
                    && aPermission.asJSONResource() != null) {
                JSONResource customPermissionEntry = aPermission
                        .asJSONResource().getProperty("custom")
                        .asJSONResource();
                if (customPermissionEntry != null) {

                    Boolean asBoolean = customPermissionEntry.getBoolean(
                            "showidat");
                    if (asBoolean != null && asBoolean)
                        showIdat = true;
                } else {
                    showIdat = true;
                }
            }
        }

        Resource locationRes = getDatabase().getResourceByIdentifier(
                (String) getProperty(OSSEVocabulary.Patient.Locations));
        location = new Location(getDatabase(), locationRes);
        location.load();

        boolean isImported = isImported();

        if (!isImported && !noMainzelliste && !((ApplicationBean) Utils.getAB()).isBridgehead()) {
            // Get a token/tempID from the Mainzelliste (used for the dslib
            // javascript resolver to query IDAT from the Mainzelliste)
            try {
                Session mlSession = ((SessionBean) Utils.getSB())
                        .getMainzellisteSession();
                ID myId = new ID("psn", getPatientID());

                List<String> fieldsToShow = null;
                
                if(((ApplicationBean) Utils.getAB()).doesMainzellisteWantIdat())
                    fieldsToShow = Arrays.asList("vorname", "nachname", "geburtstag", "geburtsmonat", "geburtsjahr");
                else
                    fieldsToShow = Arrays.asList("patientId");
                
                List<String> idsToShow = Arrays.asList("pid");
                mainzellisteTempID = mlSession.getTempId(myId, fieldsToShow,
                        idsToShow);

            } catch (MainzellisteNetworkException | InvalidSessionException e) {
                e.printStackTrace();
            }
        }

        // load user
        String userResourceId = (String) getProperty(Vocabulary.Patient.User);
        if (userResourceId != null) {
            Resource userResource = getDatabase()
                    .getResourceByIdentifier(userResourceId);
            asUser = new User(getDatabase(), userResource);
            asUser.load();
        }

        return true;
    }

    /**
     * Gets the patient id.
     *
     * @return the patient id
     */
    public String getPatientID() {
        return (String) getProperty("patientID");
    }

    /**
     * Gets the patient uri.
     *
     * @return the patient uri
     */
    public String getPatientURI() {
        return getResourceURI();
    }

    /**
     * Compares one patient to another (by their pseudonyms) TODO: As the
     * pseudonyms are Mainzelliste TempIDs now, it makes no sense to compare
     * patients based on that data. The problem is, that we don't have any data
     * stored about patients anylonger (other than their ID) to compare them to
     * each other.
     *
     * @param patient the patient
     * @return the int
     * @see de.samply.edc.model.Entity#compareTo(de.samply.edc.model.Entity)
     */
    @Override
    public int compareTo(Entity patient) {
        return this.getPseudonym()
                .compareTo(((Patient) patient).getPseudonym());
    }

    /**
     * Checks if there are cases for this patient.
     *
     * @return true, if successful
     */
    public boolean hasCases() {
        return hasChildren(OSSEVocabulary.Type.Case);
    }

    /**
     * Adds a case to this patient.
     *
     * @param newCase            the new case
     * @return the case
     */
    public Case addCase(Case newCase) {
        addChild(OSSEVocabulary.Type.Case, newCase);
        return newCase;
    }

    /**
     * Checks whether to show identifying data (IDAT) for this patient.
     *
     * @return true|false
     */
    public Boolean getShowIdat() {
        return showIdat;
    }

    /**
     * Sets whether to show identifying data (IDAT) for this patient.
     *
     * @param showIdat
     *            true|false
     */
    public void setShowIdat(Boolean showIdat) {
        this.showIdat = showIdat;
    }

    /**
     * Gets the location of this patient.
     *
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Sets the location of this patient.
     *
     * @param location
     *            the new location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Gets the user object if the patient is registered as a user.
     *
     * @return corresponding user object if the patient is registered as user;
     *         null otherwise
     */
    public User getAsUser() {
        return asUser;
    }

    /**
     * Checks if is activated user.
     *
     * @return is the patient registered as user and is the user activated?
     */
    public boolean isActivatedUser() {
        return asUser != null && asUser.isActivated();
    }

    /**
     * Was the patient imported via REST?.
     *
     * @return true, if is imported
     */
    public boolean isImported() {
        Boolean isImported = false;
        Value isImportedValue = getResource().getProperty(Vocabulary.Patient.isImported);
        if (isImportedValue != null)
            isImported = isImportedValue.asBoolean();
        if (isImported == null)
            isImported = false;

        return isImported;
    }
}
