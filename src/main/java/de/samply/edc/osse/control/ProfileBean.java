/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.control;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.osse.model.User;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;

/**
 * View-scoped bean for user profile data management.
 */
@ManagedBean
@ViewScoped
public class ProfileBean extends AbstractViewBean {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The user. */
    private User user;

    /**
     * Post-construct init.
     *
     * @see de.samply.edc.control.AbstractViewBean#init()
     */
    @Override
    public void init() {
        user = ((SessionBean) getSessionBean()).getCurrentUser();
        if (user == null) {
            // No user logged in? Might be timed out
            try {
                Utils.redirectToPage("login.xhtml");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        setFormParam("defaultRole", user.getDefaultRole());
        super.init();
    }

    /**
     * Sets the default role.
     *
     * @return the string
     */
    public String setDefaultRole() {
        String defaultRole = getFormParam("defaultRole");

        user.getResource()
                .setProperty(Vocabulary.User.defaultRole, defaultRole);
        getSessionBean().getDatabase().save(user.getResource());

        Utils.addContextMessage("Success",
                "Your default role has been selected.");
        return "";
    }

    /**
     * Change password.
     *
     * @return the string
     */
    public String changePassword() {
        String oldpass = getFormParam("currentpassword");
        String password = getFormParam("password");
        String passwordRepeat = getFormParam("password_repeat");

        // no password given
        if (password == null || password.equals("")) {
            Utils.displayContextMessage("summary_password_change_failed", "password_change_nopw");
            return "";
        }

        User user = ((SessionBean) getSessionBean()).getCurrentUser();

        String ok = Utils.validatePassword(password, passwordRepeat, user.getUsername(), user.getContact()
                .getLastname());
        if (ok != null) {
            Utils.displayContextMessage("summary_password_change_failed", ok);
            return "";
        }

        Resource userResource = getSessionBean()
                .getDatabase()
                .changeUserPassword(
                        (((SessionBean) getSessionBean()).getCurrentUser())
                                .getResource(),
                        oldpass, password);
        if (userResource != null) {
            Utils.displayContextMessage("summary_password_change_success", "success_edituser");
        } else {
            Utils.displayContextMessage("summary_password_change_failed", "password_change_failed");
            return "";
        }

        getSessionBean().getDatabase().save(userResource);

        getSessionBean().addLog("changed his password");
        return "changedPassword";
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }

}
