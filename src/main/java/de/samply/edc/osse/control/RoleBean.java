/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.model.Location;
import de.samply.edc.osse.model.Permission;
import de.samply.edc.osse.model.Role;
import de.samply.edc.osse.utils.OSSEUtils;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSERoleType;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;

/**
 * View scoped bean to manage roles.
 */
@ManagedBean
@ViewScoped
public class RoleBean extends AbstractViewBean {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * The list of roles as a HashMap, which contain the role data.
     *
     * Keys are:
     *
     * "name" - Name of the role
     *
     * "rolelocation" - Location of the role
     *
     * "permissions" - List of permission entities
     * */
    private ArrayList<HashMap<String, Object>> roles;

    /**  List of locations as SelectItems (used in JSF components). */
    private List<SelectItem> locationsSelectItems;

    /**  A comma seperated list of items (used in Javascript). */
    private String items = "";

    /**  A list of items linked to 'items' above. */
    private List<HashMap<String, String>> elements = new ArrayList<>();

    /**
     * A list of permission quickies.
     *
     * A permission quicky is basically a collection of predefined permissions
     * under a name, for example the quicky "DataEntry+" containing permissions
     * to read,write,create patients and their medical data).
     *
     */
    private List<HashMap<String, String>> allPermissionQuickies = new ArrayList<>();

    /**  A list of permissions. */
    private List<Permission> createdPermissions;

    /**
     * Post-construct init defines the permission quickies.
     *
     * @see de.samply.edc.control.AbstractViewBean#init()
     */
    @Override
    public void init() {
        items = "";
        roles = null;
        locationsSelectItems = null;
        super.init();

        // TODO: Yet a hardcoded list of quickies, in the future maybe this can
        // be also made freely configurable
        HashMap<String, String> aPermissionQuicky = new HashMap<>();
        aPermissionQuicky = new HashMap<>();
        aPermissionQuicky.put("urn", "A");
        aPermissionQuicky.put("designation", "DataEntry+");
        aPermissionQuicky
                .put("definition",
                        "Permission to add new patients and to read and enter medical data");
        allPermissionQuickies.add(aPermissionQuicky);

        aPermissionQuicky = new HashMap<>();
        aPermissionQuicky.put("urn", "B");
        aPermissionQuicky.put("designation", "DataEntry");
        aPermissionQuicky.put("definition",
                "Permission to read and enter medical data");
        allPermissionQuickies.add(aPermissionQuicky);

        aPermissionQuicky = new HashMap<>();
        aPermissionQuicky.put("urn", "C");
        aPermissionQuicky.put("designation", "DataRead");
        aPermissionQuicky.put("definition", "Permission to read data");
        allPermissionQuickies.add(aPermissionQuicky);

        aPermissionQuicky = new HashMap<>();
        aPermissionQuicky.put("urn", "L");
        aPermissionQuicky.put("designation", "See my IDAT");
        aPermissionQuicky
                .put("definition",
                        "Permission to see the IDAT of patients of your own location");
        allPermissionQuickies.add(aPermissionQuicky);

        aPermissionQuicky = new HashMap<>();
        aPermissionQuicky.put("urn", "I");
        aPermissionQuicky.put("designation", "See all IDAT");
        aPermissionQuicky
                .put("definition",
                        "Permission to see the IDAT of any patient, not only those of your own location");
        allPermissionQuickies.add(aPermissionQuicky);

        aPermissionQuicky = new HashMap<>();
        aPermissionQuicky.put("urn", "E");
        aPermissionQuicky.put("designation", "DataExport");
        aPermissionQuicky.put("definition",
                "Permission to export all medical data");
        allPermissionQuickies.add(aPermissionQuicky);

        aPermissionQuicky = new HashMap<>();
        aPermissionQuicky.put("urn", "R");
        aPermissionQuicky.put("designation", "DataReport");
        aPermissionQuicky.put("definition",
                "Permission to change the form status from open to reported");
        allPermissionQuickies.add(aPermissionQuicky);

        aPermissionQuicky = new HashMap<>();
        aPermissionQuicky.put("urn", "S");
        aPermissionQuicky.put("designation", "DataValidation");
        aPermissionQuicky
                .put("definition",
                        "Permission to change the form status from reported to validated");
        allPermissionQuickies.add(aPermissionQuicky);

        aPermissionQuicky = new HashMap<>();
        aPermissionQuicky.put("urn", "T");
        aPermissionQuicky.put("designation", "RemoveValidation");
        aPermissionQuicky
                .put("definition",
                        "Permission to change the form status from validated to open again");
        allPermissionQuickies.add(aPermissionQuicky);
        
        aPermissionQuicky = new HashMap<>();
        aPermissionQuicky.put("urn", "P");
        aPermissionQuicky.put("designation", "PatientAccounts");
        aPermissionQuicky
                .put("definition",
                        "Permission to handle patient accounts");
        allPermissionQuickies.add(aPermissionQuicky);
    }

    /**
     * Load all roles.
     */
    private void loadRoles() {
        roles = new ArrayList<HashMap<String, Object>>();

        List<Resource> resultRoles = ((Database) getSessionBean().getDatabase())
                .getRoles();
        for (Resource roleResource : resultRoles) {
            Role role = new Role(getSessionBean().getDatabase(), roleResource);
            role.load(false);

            // only display custom roles
            if (!role.isCustomRole())
                continue;

            role.loadPermissions();
            
            DatatableRows permissions = new DatatableRows();

            dataObject = new HashMap<String, Object>();
            dataObject.put("name", role.getName());
            if (role.getLocation() != null)
                dataObject.put("rolelocation", role.getLocation().getName());
            else
                dataObject.put("rolelocation", "N/A");
            dataObject.put("roleResource", roleResource);

            for (Entity permission : role.getPermissions()) {
                // do not display the create permissions as they are "included"
                // in the write permissions by definition
                if ("create".equals(((Permission) permission).getAccess())) {
                    continue;
                }

                HashMap<String, Object> columns = new HashMap<String, Object>();

                if (((Permission) permission).getLocation() != null)
                    columns.put("location", ((Permission) permission)
                            .getLocation().getName());

                columns.put("entityPatient",
                        ((Permission) permission).getEntityPatient());
                columns.put("entityCase",
                        ((Permission) permission).getEntityCase());
                columns.put("entityEpisode",
                        ((Permission) permission).getEntityEpisode());
                columns.put("entityForm",
                        ((Permission) permission).getEntityForm());
                columns.put("entityUser",
                        ((Permission) permission).getEntityUser());

                String access = ((Permission) permission).getAccess();

                // translate access to a status if we got a statusID permission
                if (((Permission) permission).getStatusID() != null) {
                    access = ((ApplicationBean) Utils.getAB()).getFormStatus()
                            .get(((Permission) permission).getStatusID());
                }

                columns.put("access", access);
                columns.put("name", ((Permission) permission).getName());
                DatatableRow permissionRow = new DatatableRow();
                permissionRow.setColumns(columns);
                permissions.add(permissionRow);
            }

            dataObject.put("permissions", permissions);
            roles.add(dataObject);
        }

        dataObject = null;
    }

    /**
     * Gets the dataObject, but also "translates" it to whatever quicky used.
     *
     * @return the data object
     * @see de.samply.edc.control.AbstractViewBean#getDataObject()
     */
    @Override
    public HashMap<String, Object> getDataObject() {
        if (dataObject == null) {
            dataObject = super.getDataObject();

            checkRoleForQuickies();
            refreshItems();
        }

        return dataObject;
    }

    /**
     * Checks a role for quickies.
     */
    public void checkRoleForQuickies() {
        Boolean readPatient = false;
        Boolean writePatient = false;
        Boolean fullMedicalReadAccess = false;
        Boolean fullMedicalWriteAccess = false;

        Boolean reporter = false;
        Boolean signer = false;
        Boolean breaker = false;
        Boolean exporter = false;
        Boolean myIdatReader = false;
        Boolean idatReader = false;
        Boolean patientAccounts = false;

        DatatableRows permissions = (DatatableRows) dataObject
                .get("permissions");
        DatatableRows temp = new DatatableRows();
        List<DatatableRow> storedPermission = new ArrayList<DatatableRow>();

        if (permissions != null && permissions.getRows() != null)
            for (DatatableRow permission : permissions.getRows()) {
                HashMap<String, Object> permissionColumns = permission
                        .getColumns();

                if ("executeAction".equals(permissionColumns.get("access"))) {
                    if("createPatientUser".equals(permissionColumns.get("name")))
                        patientAccounts = true;
                }
                
                if (permissionColumns.get("location") == null
                        || dataObject.get("rolelocation") == null)
                    continue;

                // If the role location and permission location is the same,
                // check if we got a quickie
                if (permissionColumns.get("location").equals(
                        dataObject.get("rolelocation"))) {
                    if ("write".equals(permissionColumns.get("access"))) {
                        if ((Boolean) permissionColumns.get("entityCase")
                                && (Boolean) permissionColumns
                                        .get("entityPatient")) {
                            writePatient = true;
                        }

                        if ((Boolean) permissionColumns.get("entityEpisode")
                                && (Boolean) permissionColumns
                                        .get("entityForm")) {
                            fullMedicalWriteAccess = true;
                        }

                        if (writePatient && fullMedicalWriteAccess) {
                            // we found a quickie!
                        } else if (!writePatient && fullMedicalWriteAccess) {
                            // We maybe got a partial B quickie, but store it
                            // for later in case it is not a B
                            storedPermission.add(permission);
                        } else
                            temp.add(permission);
                    } else if ("create".equals(permissionColumns.get("access"))) {
                        if ((Boolean) permissionColumns.get("entityCase")
                                && (Boolean) permissionColumns
                                        .get("entityPatient")) {
                            writePatient = true;
                        }

                        if ((Boolean) permissionColumns.get("entityEpisode")
                                && (Boolean) permissionColumns
                                        .get("entityForm")) {
                            fullMedicalWriteAccess = true;
                        }

                        if (writePatient && fullMedicalWriteAccess) {
                            // create quickie A found
                        } else
                            temp.add(permission);
                    } else if ("read".equals(permissionColumns.get("access"))) {
                        if ((Boolean) permissionColumns.get("entityCase")
                                && (Boolean) permissionColumns
                                        .get("entityPatient")) {
                            readPatient = true;
                        }

                        if ((Boolean) permissionColumns.get("entityEpisode")
                                && (Boolean) permissionColumns
                                        .get("entityForm")) {
                            fullMedicalReadAccess = true;
                        }

                        if (readPatient && fullMedicalReadAccess) {
                            // at least quickie C found
                        } else if (readPatient && fullMedicalWriteAccess) {
                            // We maybe got a partial B quickie, but store it
                            // for later in case it is not a B
                            storedPermission.add(permission);
                        } else
                            temp.add(permission);
                    } else if ("open".equals(permissionColumns.get("access"))) {
                        reporter = true;
                    } else if ("reported".equals(permissionColumns
                            .get("access"))) {
                        signer = true;
                    } else if ("validated".equals(permissionColumns.get("access"))) {
                        breaker = true;
                    } else if ("export".equals(permissionColumns.get("access"))) {
                        exporter = true;
                    } else if ("myIdatReader".equals(permissionColumns.get("access"))) {
                        myIdatReader = true;
                    } else if ("idatReader".equals(permissionColumns.get("access"))) {
                        idatReader = true;
                    } else
                        temp.add(permission);
                }
            }

        ArrayList<String> myItems = new ArrayList<>();

        if (writePatient) {
            if (fullMedicalWriteAccess) {
                myItems.add("A");
            }
        } else if (readPatient) {
            if (fullMedicalWriteAccess) {
                myItems.add("B");
            } else if (fullMedicalReadAccess) {
                myItems.add("C");
            }
        }

        if (!myItems.contains("B")) {
            for (DatatableRow permission : storedPermission) {
                temp.add(permission);
            }
        }

        if (reporter) {
            if (myItems.isEmpty())
                myItems.add("C");
            myItems.add("R");
        }
        if (signer) {
            if (myItems.isEmpty())
                myItems.add("C");
            myItems.add("S");
        }
        if (breaker) {
            if (myItems.isEmpty())
                myItems.add("C");
            myItems.add("T");
        }
        if (exporter) {
            if (myItems.isEmpty())
                myItems.add("C");
            myItems.add("E");
        }
        if (idatReader) {
            if (myItems.isEmpty())
                myItems.add("C");
            myItems.add("I");
        }
        if (myIdatReader && !idatReader) {
            if (myItems.isEmpty())
                myItems.add("C");
            myItems.add("L");
        }
        if (patientAccounts) {
            if (myItems.isEmpty())
                myItems.add("C");
            myItems.add("P");
        }
        items = StringUtils.join(myItems, ",");

        dataObject.put("permissions", temp);
    }

    /**
     * Returns a list of select items of available locations. Used in
     * JSF-Selects.
     *
     * @return the location list
     */
    public List<SelectItem> getLocationList() {
        if (locationsSelectItems == null) {
            locationsSelectItems = OSSEUtils.getLocationsAsSelectItems((Database) getSessionBean().getDatabase());
        }
        return locationsSelectItems;
    }

    /**
     * Returns a list of select items of available modes. Used in JSF-Selects.
     *
     * @return the mode list
     */
    public List<SelectItem> getModeList() {
        List<SelectItem> selectItemsModes = new ArrayList<SelectItem>();

        SelectItem item;
        ArrayList<String> modes = new ArrayList<String>();
        modes.add("");

        // When write then add automatic create, so we won't make create
        // available by itself
        modes.add("write");
        modes.add("read");

        // special rights for forms (transitions) - not yet used
        // HashMap<String, HashMap<String, String>> transitionNames =
        // ((ApplicationBean) Utils.getAB()).getRulenames();
        // for(String transitionName : transitionNames.keySet()) {
        // modes.add(transitionName);
        // }

        // special rights for forms (status)
        HashMap<Integer, String> formStatus = ((ApplicationBean) Utils.getAB())
                .getFormStatus();
        for (Integer statusID : formStatus.keySet()) {
            modes.add(formStatus.get(statusID));
        }

        for (String mode : modes) {
            item = new SelectItem();
            item.setLabel(mode);
            item.setValue(mode);
            selectItemsModes.add(item);
        }

        return selectItemsModes;
    }

    /**
     * Saves role data.
     *
     * @return the string
     * @see de.samply.edc.control.AbstractViewBean#save()
     */
    @Override
    public String save() {
        createdPermissions = new ArrayList<Permission>();

        String roleName = (String) dataObject.get("name");
        String roleLocationName = (String) dataObject.get("rolelocation");
        DatatableRows permissions = (DatatableRows) dataObject
                .get("permissions");
        Resource roleResource = (Resource) dataObject.get("roleResource");

        Role role = new Role(getSessionBean().getDatabase());
        String error_summary = Utils
                .getResourceBundleString("addrole_summary_fail");

        // check if we add a new role (then roleResource is null), or we've
        // changed our own name
        if (roleResource != null
                && !roleResource.getProperty(OSSEVocabulary.Role.Name)
                        .getValue().equalsIgnoreCase(roleName)) {
            // check if that new name already exists
            if (role.entityExistsByProperty(OSSEVocabulary.Role.Name, roleName) != null) {
                String[] replaceArray = { roleName };
                String error_message = Utils
                        .getResourceBundleStringWithPlaceholders(
                                "addrole_role_exists", replaceArray);

                Utils.addContextMessage(error_summary, error_message);
                return "";
            }
        } else if (roleResource == null) {
            // check if that new name already exists
            if (role.entityExistsByProperty(OSSEVocabulary.Role.Name, roleName) != null) {
                String[] replaceArray = { roleName };
                String error_message = Utils
                        .getResourceBundleStringWithPlaceholders(
                                "addrole_role_exists", replaceArray);

                Utils.addContextMessage(error_summary, error_message);
                return "";
            }
        }

        getSessionBean().getDatabase().beginTransaction();

        String summary = Utils
                .getResourceBundleString("addrole_summary_success");
        String[] replaceArray = { roleName };
        String report = Utils.getResourceBundleStringWithPlaceholders(
                "addrole_save", replaceArray);

        if (roleResource != null) {
            role.setResource(roleResource);
            report = Utils.getResourceBundleStringWithPlaceholders(
                    "editrole_save", replaceArray);

            // Delete all old permissions
            ArrayList<Resource> permissionResources = roleResource
                    .getResources(OSSEVocabulary.Role.ReadOnly.Permissions,
                            getSessionBean().getDatabase().getDatabaseModel());
            if (permissionResources != null && !permissionResources.isEmpty())
                getSessionBean().getDatabase().deleteResource(
                        permissionResources
                                .toArray(new Resource[permissionResources
                                        .size()]));
        }

        role.setProperty(OSSEVocabulary.Role.Name, roleName);

        Location roleLocation = new Location(getSessionBean().getDatabase());
        Resource roleLocationResource = roleLocation.entityExistsByProperty(
                OSSEVocabulary.Location.Name, roleLocationName);
        role.setProperty(OSSEVocabulary.Role.Location, roleLocationResource);

        role.setProperty(OSSEOntology.Role.RoleType, OSSERoleType.CUSTOM);
        role.setProperty(OSSEOntology.Role.IsUserRole, false);

        role.saveOrUpdate();
        if (roleResource == null)
            roleResource = role.getResource();

        if (permissions != null) {
            for (DatatableRow permissionRow : permissions.getRows()) {
                String access = (String) permissionRow.getColumns().get(
                        "access");

                // Check if access from the form is a statusname
                Integer statusID = ((ApplicationBean) Utils.getAB())
                        .getFormStatusReverse().get(access);
                if (statusID != null) {
                    access = "executeAction";
                    if (!createPermission(
                            role,
                            access,
                            (Boolean) permissionRow.getColumns().get(
                                    "entityPatient"),
                            (Boolean) permissionRow.getColumns().get(
                                    "entityCase"),
                            (Boolean) permissionRow.getColumns().get(
                                    "entityEpisode"),
                            (Boolean) permissionRow.getColumns().get(
                                    "entityForm"), (String) permissionRow
                                    .getColumns().get("location"), statusID)) {
                        Utils.addContextMessage(
                                error_summary,
                                Utils.getResourceBundleString("addrole_cannot_save_permission"));
                        return null;
                    }
                } else {
                    if (!createPermission(
                            role,
                            access,
                            (Boolean) permissionRow.getColumns().get(
                                    "entityPatient"),
                            (Boolean) permissionRow.getColumns().get(
                                    "entityCase"),
                            (Boolean) permissionRow.getColumns().get(
                                    "entityEpisode"),
                            (Boolean) permissionRow.getColumns().get(
                                    "entityForm"), (String) permissionRow
                                    .getColumns().get("location"))) {
                        Utils.addContextMessage(
                                error_summary,
                                Utils.getResourceBundleString("addrole_cannot_save_permission"));
                        return null;
                    }

                    // if we add the write right to an entity, we automatically
                    // add the create right, too
                    if ("write".equalsIgnoreCase((String) permissionRow
                            .getColumns().get("access"))) {
                        if (!createPermission(
                                role,
                                "create",
                                (Boolean) permissionRow.getColumns().get(
                                        "entityPatient"),
                                (Boolean) permissionRow.getColumns().get(
                                        "entityCase"),
                                (Boolean) permissionRow.getColumns().get(
                                        "entityEpisode"),
                                (Boolean) permissionRow.getColumns().get(
                                        "entityForm"), (String) permissionRow
                                        .getColumns().get("location")))
                            return null;
                    }
                }
            }
        }

        // Save quickies
        String[] items = this.items.split(",");
        List<String> moo = Arrays.asList(items);
        Collections.sort(moo);
        Boolean higherDone = false;

        Boolean doAddIdatReader = false;
        Boolean doAddMyIdatReader = false;
        
        for (String item : moo) {
            if (item.equalsIgnoreCase("A")) {
                createPermission(role, "read", true, true, true, true,
                        roleLocationName);
                createPermission(role, "write", true, true, true, true,
                        roleLocationName);
                createPermission(role, "create", true, true, true, true,
                        roleLocationName);

                createPermission(role, "read", true, true, true, true, null);
                higherDone = true;
            } else if (item.equalsIgnoreCase("B") && !higherDone) {
                createPermission(role, "read", true, true, true, true,
                        roleLocationName);
                createPermission(role, "write", false, false, true, true,
                        roleLocationName);
                createPermission(role, "create", false, false, true, true,
                        roleLocationName);

                createPermission(role, "read", true, true, true, true, null);
                higherDone = true;
            } else if (item.equalsIgnoreCase("C") && !higherDone) {
                createPermission(role, "read", true, true, true, true,
                        roleLocationName);

                createPermission(role, "read", true, true, true, true, null);
            } else if (item.equalsIgnoreCase("R")) {
                createPermission(role, "executeAction", false, false, false,
                        false, roleLocationName, 1);
            } else if (item.equalsIgnoreCase("S")) {
                createPermission(role, "executeAction", false, false, false,
                        false, roleLocationName, 2);
            } else if (item.equalsIgnoreCase("T")) {
                createPermission(role, "executeAction", false, false, false,
                        false, roleLocationName, 3);
            } else if (item.equalsIgnoreCase("E")) {
                createPermission(role, "export", false, false, false, false,
                        roleLocationName);
            } else if (item.equalsIgnoreCase("I")) {
                doAddIdatReader = true;
            } else if (item.equalsIgnoreCase("L")) {
                doAddMyIdatReader = true;
            } else if (item.equalsIgnoreCase("P")) {
                createPatientUserPermission(role, roleLocationName);
            }
        }

        if(doAddIdatReader) {
            createPermission(role, "idatReader", false, false, false, false,
                    roleLocationName);
        } else if(doAddMyIdatReader) {
            createPermission(role, "myIdatReader", false, false, false, false,
                    roleLocationName);
        }
        
        getSessionBean().getDatabase().commit();

        Utils.addContextMessage(summary, report);

        goRoleAdminList();
        return null;
    }

    /**
     * Check if a permission is already saved so we don't save it twice.
     *
     * @param permission
     *            the permission
     * @return the boolean
     */
    private Boolean checkAlreadySaved(Permission permission) {
        for (Permission done : createdPermissions) {
            if (done.equals(permission)) {
                Utils.getLogger().debug(
                        "\nPermission:" + permission + "\nequals    :" + done);
                return true;
            }
        }

        return false;
    }

    /**
     * Creates a permission.
     *
     * @param role
     *            the role
     * @param access
     *            the access
     * @param entityPatient
     *            the entity patient
     * @param entityCase
     *            the entity case
     * @param entityEpisode
     *            the entity episode
     * @param entityForm
     *            the entity form
     * @param location
     *            the location
     * @return the boolean
     */
    private Boolean createPermission(Role role, String access,
            Boolean entityPatient, Boolean entityCase, Boolean entityEpisode,
            Boolean entityForm, String location) {
        return createPermission(role, access, entityPatient, entityCase,
                entityEpisode, entityForm, location, null);
    }

    /**
     * Creates the patient user permission.
     *
     * @param role the role
     * @param location the location
     */
    private void createPatientUserPermission(Role role, String location) {
        Permission permission = new Permission(getSessionBean().getDatabase());
        permission.setName("createPatientUser");
        permission.setAccess("executeAction");
        permission.setEntityUser(true);
        permission.setParent(role);
        
        Location permissionLocation = new Location(getSessionBean()
                .getDatabase());
        Resource permissionLocationResource = permissionLocation
                .entityExistsByProperty(OSSEVocabulary.Location.Name,
                        location);

        if (permissionLocationResource == null) {
            getSessionBean().getDatabase().rollback();
            throw new Error("Meh, Location not found");
        }
        permissionLocation.setResource(permissionLocationResource);
        permissionLocation.setProperty(OSSEVocabulary.Location.Name,
                location);
        permission.setLocation(permissionLocation);
        
        if (!checkAlreadySaved(permission)) {
            permission.saveOrUpdate();
            createdPermissions.add(permission);
        }
    }
    
    /**
     * Creates a permission.
     *
     * @param role
     *            the role
     * @param access
     *            the access
     * @param entityPatient
     *            the entity patient
     * @param entityCase
     *            the entity case
     * @param entityEpisode
     *            the entity episode
     * @param entityForm
     *            the entity form
     * @param location
     *            the location
     * @param statusID
     *            the status id
     * @return the boolean
     */
    private Boolean createPermission(Role role, String access,
            Boolean entityPatient, Boolean entityCase, Boolean entityEpisode,
            Boolean entityForm, String location, Integer statusID) {
        // TODO: check if the "same" permission is already saved, then don't
        // save another one (no doubles please)

        Permission permission = new Permission(getSessionBean().getDatabase());
        
        permission.setStatusID(statusID);
        if(statusID != null)
            permission.setName("changeStatus");
        
        permission.setAccess(access);
        permission.setEntityPatient(entityPatient);
        // TODO: For now if the user may do stuff about the patient, he may also
        // do the same about the cases
        permission.setEntityCase(entityPatient);

        permission.setEntityEpisode(entityEpisode);
        permission.setEntityForm(entityForm);

        if (location == null) {
            permission.setShowIdat(false);
        } else {
            permission.setShowIdat(true);

            Location permissionLocation = new Location(getSessionBean()
                    .getDatabase());
            Resource permissionLocationResource = permissionLocation
                    .entityExistsByProperty(OSSEVocabulary.Location.Name,
                            location);

            if (permissionLocationResource == null) {
                getSessionBean().getDatabase().rollback();
                throw new Error("Meh, Location not found");
            }
            permissionLocation.setResource(permissionLocationResource);
            permissionLocation.setProperty(OSSEVocabulary.Location.Name,
                    location);
            permission.setLocation(permissionLocation);
        }

        permission.setParent(role);

        if (!checkAlreadySaved(permission)) {
            permission.saveOrUpdate();
            createdPermissions.add(permission);
        }

        return true;
    }

    /**
     * Redirect user to the role admin list.
     */
    public void goRoleAdminList() {
        Utils.goAdminForm("rolelist");
    }

    /**
     * JSF action after pressing edit of a selected role.
     *
     * @param roleData
     *            the role data
     * @return the string
     */
    public String editRole(HashMap<String, Object> roleData) {
        getSessionBean().setTempObject("dataObject", roleData);
        Utils.goAdminForm("role_edit");
        return "editRole";
    }

    /**
     * redirects the user when wanting to add a new role.
     *
     * @return the string
     */
    public String goAddRole() {
        getSessionBean().clearTempObject("dataObject");
        Utils.goAdminForm("role_edit");
        return "addRole";
    }

    /**
     * Sets the role to be deleted.
     *
     * @param toDeleteRole            the new to delete role
     */
    public void setToDeleteRole(Object toDeleteRole) {
        getSessionBean().setTempObject("dataObject", toDeleteRole);
    }

    /**
     * Deletes a role.
     *
     * @return the string
     */
    @SuppressWarnings("unchecked")
    public String deleteRole() {
        dataObject = (HashMap<String, Object>) getSessionBean()
                .clearTempObject("dataObject");
        if (dataObject == null)
            return "";

        ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Role);
        query.add(Criteria.Equal(OSSEVocabulary.Type.Role,
                OSSEVocabulary.Role.Name,
                (String) dataObject.get(OSSEVocabulary.Role.Name)));
        ArrayList<Resource> found = getSessionBean().getDatabase()
                .getResources(query);
        for (Resource deleteMe : found) {
            // TODO: delete permission first, if it's not used in another role
            getSessionBean().getDatabase().deleteResource(deleteMe);
        }

        loadRoles();
        return "deleteRole";
    }

    /**
     * Loads the javascript items list and puts its data into the java elements
     * list.
     */
    public void refreshItems() {
        String[] items = this.items.split(",");
        elements = new ArrayList<>();

        for (String s : items) {
            if (s.trim().length() == 0) {
                continue;
            }

            elements.add(findElement(s));
        }
    }

    /**
     * Finds an element by its "urn" property, and returns the element as a
     * HashMap containing its data.
     *
     * @param searchFor            the urn property to search for
     * @return the hash map
     */
    private HashMap<String, String> findElement(String searchFor) {
        for (HashMap<String, String> element : allPermissionQuickies) {
            if (searchFor.equals(element.get("urn"))) {
                return element;
            }
        }

        return null;
    }

    /**
     * Gets the roles.
     *
     * @return the roles
     */
    public ArrayList<HashMap<String, Object>> getRoles() {
        if (roles == null)
            loadRoles();
        return roles;
    }

    /**
     * Gets the items. A comma seperated list of items (used in Javascript)
     *
     * @return the items
     */
    public String getItems() {
        return items;
    }

    /**
     * Sets the items. A comma seperated list of items (used in Javascript)
     *
     * @param items
     *            the new items
     */
    public void setItems(String items) {
        this.items = items;
    }

    /**
     * Gets the elements. A list of elements linked to 'items' above
     *
     * @return the elements
     * @see findElement for more info about the elements
     */
    public List<HashMap<String, String>> getElements() {
        return elements;
    }

    /**
     * Sets the elements. A list of items linked to 'items' above
     *
     * @param elements            the elements
     * @see findElement for more info about the elements
     */
    public void setElements(List<HashMap<String, String>> elements) {
        this.elements = elements;
    }

    /**
     * Gets all permission quickies.
     *
     * @return the list of all permission quickies
     * @see init for more info about the HashMap
     */
    public List<HashMap<String, String>> getAllPermissionQuickies() {
        return allPermissionQuickies;
    }

    /**
     * Sets all permission quickies.
     *
     * @param allPermissionQuickies            the list of all permission quickies
     * @see init for more info about the HashMap
     */
    public void setAllPermissionQuickies(List<HashMap<String, String>> allPermissionQuickies) {
        this.allPermissionQuickies = allPermissionQuickies;
    }

}
