/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.control;

import java.util.ArrayList;
import java.util.List;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.control.AbstractSessionBean;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEModel;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;

/**
 * The database wrapper class.
 */
public class Database extends AbstractDatabase<OSSEModel> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new database.
     *
     * @param sessionBeanBase
     *            the session bean base
     */
    public Database(AbstractSessionBean sessionBeanBase) {
        super(sessionBeanBase);
    }

    /**
     * Instantiates a new database.
     *
     * @param isSystemAction the is system action
     */
    public Database(Boolean isSystemAction) {
        super(isSystemAction);
    }

    /*
     * (non-Javadoc)
     *
     * @see de.samply.edc.control.AbstractDatabase#get(java.lang.String)
     */
    @Override
    protected OSSEModel get(String file) {
        try {
            return new OSSEModel(file);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Wipes a patient completely (hard delete).
     *
     * @param patientResource
     *            the patient resource
     */
    public void wipeoutPatient(Resource patientResource) {
        try {
            ((OSSEModel) getDatabaseModel()).wipeoutPatient(patientResource);
        } catch (DatabaseException e) {
            Utils.getLogger()
                    .debug("EXCEPTION in Database wrapper wipeoutPatient with patRes=" + patientResource + "!");
            e.printStackTrace();
        }
    }

    /**
     * Wipes a resource completely (hard delete).
     *
     * @param killme
     *            the resource to be wiped
     */
    public void wipeoutResource(Resource killme) {
        try {
            ((OSSEModel) getDatabaseModel()).wipeoutResource(killme);
        } catch (DatabaseException e) {
            Utils.getLogger()
                    .debug("EXCEPTION in Database wrapper wipeoutResource with wipeoutResource=" + killme + "!");
            e.printStackTrace();
        }
    }
    
    /**
     * Selects a role for the current session.
     *
     * @param role
     *            the role
     */
    public void selectRole(Resource role) {
        try {
            getDatabaseModel().selectRole(role.getId());
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets a list of role resources the user can choose Either all, or searched
     * for the given name.
     *
     * @param roleName            The role to search for
     * @return list or role resources
     */
    public List<Resource> getRoles(String roleName) {
        try {
            ResourceQuery queryRole = new ResourceQuery(OSSEVocabulary.Type.Role);

            // User roles are roles that are not selectable or visible
            // and just there to bind more permissions to a special user
            queryRole.add(Criteria.Equal(OSSEVocabulary.Type.Role, OSSEOntology.Role.IsUserRole, false));

            if (roleName != null)
                queryRole.add(Criteria.Equal(OSSEVocabulary.Type.Role, OSSEVocabulary.Role.Name, roleName));
            return getDatabaseModel().getResources(queryRole);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        return new ArrayList<Resource>();
    }

    /**
     * Gets the roles the current user can actually choose.
     *
     * @return list of role resources
     */
    public List<Resource> getRoles() {
        return getRoles(null);
    }

    /**
     * Gets a list of location resources without the "Global Roles" location,
     * which is used internally to store certain systemwide roles and is not a
     * "user custom" location If you set a locationName, that will be searched
     * for.
     *
     * @param locationName            The name of the location to find (if null, all locations are
     *            returned)
     * @return list of location resources
     */
    public List<Resource> getLocations(String locationName) {
        try {
            ResourceQuery queryLocations = new ResourceQuery(OSSEVocabulary.Type.Location);

            // global roles is a system location that we want to ignore here
            queryLocations.add(Criteria.NotEqual(OSSEVocabulary.Type.Location, OSSEVocabulary.Location.Name,
                    Vocabulary.generic.systemLocation));

            if (locationName != null)
                queryLocations
                        .add(Criteria.Equal(OSSEVocabulary.Type.Location, OSSEVocabulary.Role.Name, locationName));

            return getDatabaseModel().getResources(queryLocations);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        return new ArrayList<Resource>();
    }

    /**
     * Gets a list of location resources without the "Global Roles" location,
     * which is used internally to store certain systemwide roles and is not a
     * "user custom" location.
     *
     * @return list of location resources
     */
    public List<Resource> getLocations() {
        return getLocations(null);
    }

    /**
     * Creates a patient user. This is used for the usecase that a documentary
     * wants to grant a patient access to his patientforms.
     *
     * @param caseResource the case resource
     * @param username the username
     * @param password the password
     * @return the resource
     * @throws DatabaseException the database exception
     */
    public Resource createPatientUser(Resource caseResource, String username, String password)
            throws DatabaseException {
        Resource userResource = ((OSSEModel) getDatabaseModel()).createPatientUser(caseResource, username, password);
        return userResource;
    }

    /**
     * Changes the password of a patient user. 
     *
     * @param userResource the user resource
     * @param password the password
     * @throws DatabaseException the database exception
     */
    public void changePatientUserPassword(Resource userResource, String password)
            throws DatabaseException {
        ((OSSEModel) getDatabaseModel()).changePatientUserPassword(userResource, password);
    }
    
    /**
     * Gets the query Result by ID.
     *
     * @param queryResultId the ID
     * @return  List<Resource>  List of patientResources
     * @throws DatabaseException the database exception
     */
    public List<Resource> getQueryResult(Integer queryResultId) throws DatabaseException {
        return ((OSSEModel) getDatabaseModel()).getQueryResult(queryResultId, true);
    }
}
