/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.auth;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.HashMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.configuration.Configuration;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.rest.AccessTokenDTO;
import de.samply.auth.rest.AccessTokenRequestDTO;
import de.samply.auth.rest.ClientDescriptionDTO;
import de.samply.auth.rest.ClientListDTO;
import de.samply.auth.rest.KeyIdentificationDTO;
import de.samply.auth.rest.LoginDTO;
import de.samply.auth.rest.LoginRequestDTO;
import de.samply.auth.rest.OAuth2Key;
import de.samply.auth.rest.OAuth2Keys;
import de.samply.auth.rest.RegistrationDTO;
import de.samply.auth.rest.RegistrationRequestDTO;
import de.samply.auth.rest.SignRequestDTO;
import de.samply.auth.rest.Usertype;
import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.utils.Utils;

/**
 * Class for communication with Samply.AUTH component
 *
 */
public abstract class Auth {

    /**
     * Generates an RSA keypair.
     *
     * @return HashMap contains private (Vocabulary.Config.Auth.MyPrivKey) and
     *         public (Vocabulary.Config.Auth.MyPubKey) key
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     */
    public static HashMap<String, String> generateKeyPair()
            throws NoSuchAlgorithmException {
        HashMap<String, String> keyMap = new HashMap<>();

        KeyPairGenerator fac = KeyPairGenerator.getInstance("RSA");
        fac.initialize(4096);
        KeyPair keyPair = fac.generateKeyPair();

        PublicKey pubkey = keyPair.getPublic();
        PrivateKey privkey = keyPair.getPrivate();
        String pubkeyString = Base64.encodeBase64String(pubkey.getEncoded());
        String privkeyString = Base64.encodeBase64String(privkey.getEncoded());

        keyMap.put(Vocabulary.Config.Auth.MyPrivKey, privkeyString);
        keyMap.put(Vocabulary.Config.Auth.MyPubKey, pubkeyString);

        return keyMap;
    }

    /**
     * Registers a registry to Samply.AUTH component
     *
     * @param pubkeyString            The public key of the Registry
     * @param email            The email address provided by the admin
     * @param registryName            The name of this Registry
     * @param configuration the configuration
     * @param usertype            The type of registry (Usertype.OSSE_REGISTRY, Usertype.BRIDGEHEAD)
     * @return HashMap contains: (Vocabulary.Config.Auth.ServerPubKey) server
     *         public key, (Vocabulary.Config.Auth.KeyId) the registry account
     *         ID, (Vocabulary.Config.Auth.UserId) the registry account user ID,
     *         or an error reason
     */
    public static HashMap<String, String> registerAuth(String pubkeyString,
            String email, String registryName, Configuration configuration, Usertype usertype) {

        if (pubkeyString == null || "".equals(pubkeyString))
            return null;

        if (email == null || "".equals(email))
            return null;

        if (registryName == null || "".equals(registryName))
            return null;

        RegistrationRequestDTO regRequest = new RegistrationRequestDTO();
        regRequest.setBase64EncodedPublicKey(pubkeyString);
        regRequest.setDescription(registryName);
        regRequest.setEmail(email);
        regRequest.setName(registryName);
        regRequest.setContactData("");
        regRequest.setUsertype(usertype);

        String authURL = configuration
                .getString(Vocabulary.Config.Auth.REST);

        HttpConnector43 hc = null;
        try {
            hc = new HttpConnector43(configuration);
        } catch (HttpConnectorException e) {
            e.printStackTrace();
        }

        if (hc == null) {
            return null;
        }

        /*
         * Usually a new user/registry account on Samply.AUTH has to be
         * activated, but for now it will always be activated
         *
         * Errorcode 404 means, the account was not activated yet (unused yet)
         * Errorcode 409 means, another registry was registered under the given
         * email adress
         */
        HashMap<String, String> authMap = new HashMap<>();

        // first check if we have an internet connection at all
        if (!Utils.checkHostReachable(hc, authURL)) {
            authMap.put("hostunreachable", "true");
            return authMap;
        }

        Client client = hc.getJerseyClientForHTTPS();

        ClientResponse response = client.resource(authURL + "/oauth2/register")
                .accept("application/json").type("application/json")
                .post(ClientResponse.class, regRequest);

        if (response.getStatus() == 409) {
            // Conflict means the email adress is already in use!
            authMap.put("conflict", "true");
            return authMap;
        }

        if (response.getStatus() != 200) {
            Utils.getLogger().error(
                    "Error code = " + response.getStatus() + " Info: "
                            + response.getStatusInfo());
            return null;
        }

        RegistrationDTO reg = response.getEntity(RegistrationDTO.class);

        Integer keyId = reg.getKeyId();
        Integer userId = reg.getUserId();

        authMap.put(Vocabulary.Config.Auth.KeyId, keyId.toString());
        authMap.put(Vocabulary.Config.Auth.UserId, userId.toString());

        // Get and store pubkey of Auth server for future signature checks
        OAuth2Keys keys = client.resource(authURL + "/oauth2/certs").get(
                OAuth2Keys.class);

        OAuth2Key key = keys.getKeys().get(0);

        authMap.put(Vocabulary.Config.Auth.ServerPubKey, key.getDerFormat());

        return authMap;
    }

    /**
     * Loads a list of all clients of the Samply.AUTH component
     *
     * @param client            A Jersey client
     * @param configuration the configuration
     * @return HashMap<String, ClientDescriptionDTO> The clientID, and its
     *         description
     */
    public static HashMap<String, ClientDescriptionDTO> loadAllClients(
            Client client, Configuration configuration) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/xml");
        HashMap<String, ClientDescriptionDTO> allClients = new HashMap<String, ClientDescriptionDTO>();
        String authURL = configuration
                .getString(Vocabulary.Config.Auth.REST);
        ClientListDTO clientList = client.resource(authURL + "/oauth2/clients")
                .accept("application/json").type("application/json")
                .get(ClientListDTO.class);

        for (ClientDescriptionDTO clientDescription : clientList.getClients()) {
            allClients.put(clientDescription.getClientId(), clientDescription);
        }

        return allClients;
    }

    /**
     * Returns a LoginDTO for MDRFaces.
     *
     * @param clientId            The clientID of the MDR
     * @param configuration the configuration
     * @return LoginDTO
     * @throws InvalidKeyException             the invalid key exception
     * @throws NoSuchAlgorithmException             the no such algorithm exception
     * @throws SignatureException             the signature exception
     * @throws JWTException             the JWT exception
     */
    public static LoginDTO getClientCommunicationCode(String clientId, Configuration configuration)
            throws InvalidKeyException, NoSuchAlgorithmException,
            SignatureException, JWTException {
        HttpConnector43 hc = null;
        try {
            hc = new HttpConnector43(configuration);
        } catch (HttpConnectorException e) {
            e.printStackTrace();
        }

        if (hc == null) {
            return null;
        }

        Client client = hc.getJerseyClientForHTTPS();

        AccessTokenDTO accessToken = getAccessToken(client, configuration);

        LoginRequestDTO loginRequest = new LoginRequestDTO();
        loginRequest.setClientId(clientId);
        String authURL = configuration
                .getString(Vocabulary.Config.Auth.REST);
        LoginDTO clientCode = client.resource(authURL + "/oauth2/login")
                .header("Authorization", accessToken.getHeader())
                .accept("application/json").type("application/json")
                .post(LoginDTO.class, loginRequest);

        client.destroy();
        hc.closeClients();

        return clientCode;
    }

    /**
     * Gets an AccessTokenDTO accessToken from Samply.AUTH component, used for
     * communication with other components
     *
     * @param client            A Jersey client
     * @param configuration the configuration
     * @return AccessTokenDTO
     * @throws NoSuchAlgorithmException             the no such algorithm exception
     * @throws InvalidKeyException             the invalid key exception
     * @throws SignatureException             the signature exception
     * @throws JWTException             the JWT exception
     */
    public static AccessTokenDTO getAccessToken(Client client, Configuration configuration)
            throws NoSuchAlgorithmException, InvalidKeyException,
            SignatureException, JWTException {
        if (client == null)
            return null;

        KeyIdentificationDTO identification = new KeyIdentificationDTO();

        String keyId = configuration
                .getString(Vocabulary.Config.Auth.KeyId);
        if (keyId == null || "".equalsIgnoreCase(keyId))
            return null;

        identification.setKeyId(Integer.parseInt(keyId));
        String authURL = configuration
                .getString(Vocabulary.Config.Auth.REST);
        ClientResponse response = client
                .resource(authURL + "/oauth2/signRequest")
                .accept("application/json").type("application/json")
                .post(ClientResponse.class, identification);
        if (response.getStatus() != 200) {
            Utils.getLogger().error(
                    "Auth.getAccessToken returned " + response.getStatus()
                            + " on signRequest bailing out!");
            return null;
        }
        SignRequestDTO signRequest = response.getEntity(SignRequestDTO.class);

        /**
         * Sign the code and encode to base64
         */
        Signature sig = Signature.getInstance(signRequest.getAlgorithm());
        PrivateKey privkey = KeyLoader.loadPrivateKey(configuration
                .getString(Vocabulary.Config.Auth.MyPrivKey));
        sig.initSign(privkey);
        sig.update(signRequest.getCode().getBytes());
        String signature = Base64.encodeBase64String(sig.sign());

        AccessTokenRequestDTO accessRequest = new AccessTokenRequestDTO();
        accessRequest.setCode(signRequest.getCode());
        accessRequest.setSignature(signature);

        response = client.resource(authURL + "/oauth2/access_token")
                .accept("application/json").type("application/json")
                .post(ClientResponse.class, accessRequest);

        if (response.getStatus() != 200) {
            Utils.getLogger().error(
                    "Auth.getAccessToken returned " + response.getStatus()
                            + " bailing out!");
            return null;
        }

        AccessTokenDTO accessToken = response.getEntity(AccessTokenDTO.class);

        return accessToken;
    }

}
