/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.dto.formeditor;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * DTO for FormDetails (used in communication with form repository REST).
 */
@XmlRootElement
public class FormDetails {

    /** The id. */
    private int id;

    /** The version. */
    private int version;

    /** The name. */
    private String name;

    /** The description. */
    private String description;

    /** The type. */
    private Type type;

    /** The items. */
    private ArrayList<Item> items = new ArrayList<FormDetails.Item>();

    /**
     * The Class Item.
     */
    public static class Item {

        /** The mdr id. */
        private String mdrId;

        /** The position. */
        private int position;

        /** The label. */
        private String label;

        /** The repeatable. */
        private boolean repeatable;

        /** The mandatory. */
        private boolean mandatory;

        /** The html. */
        private String html;

        /**
         * Gets the mdr id.
         *
         * @return the mdr id
         */
        public String getMdrId() {
            return mdrId;
        }

        /**
         * Sets the mdr id.
         *
         * @param mdrId
         *            the new mdr id
         */
        public void setMdrId(String mdrId) {
            this.mdrId = mdrId;
        }

        /**
         * Gets the position.
         *
         * @return the position
         */
        public int getPosition() {
            return position;
        }

        /**
         * Sets the position.
         *
         * @param position
         *            the new position
         */
        public void setPosition(int position) {
            this.position = position;
        }

        /**
         * Gets the label.
         *
         * @return the label
         */
        public String getLabel() {
            return label;
        }

        /**
         * Sets the label.
         *
         * @param label
         *            the new label
         */
        public void setLabel(String label) {
            this.label = label;
        }

        /**
         * Checks if is repeatable.
         *
         * @return true, if is repeatable
         */
        public boolean isRepeatable() {
            return repeatable;
        }

        /**
         * Sets the repeatable.
         *
         * @param repeatable
         *            the new repeatable
         */
        public void setRepeatable(boolean repeatable) {
            this.repeatable = repeatable;
        }

        /**
         * Checks if is mandatory.
         *
         * @return true, if is mandatory
         */
        public boolean isMandatory() {
            return mandatory;
        }

        /**
         * Sets the mandatory.
         *
         * @param mandatory
         *            the new mandatory
         */
        public void setMandatory(boolean mandatory) {
            this.mandatory = mandatory;
        }

        /**
         * Gets the html.
         *
         * @return the html
         */
        public String getHtml() {
            return html;
        }

        /**
         * Sets the html.
         *
         * @param html
         *            the new html
         */
        public void setHtml(String html) {
            this.html = html;
        }
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public Type getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the new type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * Gets the items.
     *
     * @return the items
     */
    public ArrayList<Item> getItems() {
        return items;
    }

    /**
     * Sets the items.
     *
     * @param items
     *            the new items
     */
    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the new version
     */
    public void setVersion(int version) {
        this.version = version;
    }

}
