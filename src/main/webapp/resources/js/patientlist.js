/**
 * Sorting plug-in for DataTables "yesnoidat" 
 * sorts fields that have no 'data-subject="vorname' defined to the end of the table
 * 
 * @name: patientlist no-idat sorter
 * @summary: sorts fields that have no 'data-subject="vorname' defined to the end of the table
 * @example
 *   $('#mytable').dataTable({
 *      columnDefs: [
 *        { targets: 0, type: 'yesnoidat' }
 *      ]
 *   });
 */ 

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
	"yesnoidat-asc" : function(str1, str2) {
		if(str1.indexOf("data-subject=\"vorname") == -1) {
            return 1;
		}
		if(str2.indexOf("data-subject=\"vorname") == -1) {
            return -1;
		}
        return ((str1 < str2) ? -1 : ((str1 > str2) ? 1 : 0));
	},
	"yesnoidat-desc" : function(str1, str2) {
		if(str1.indexOf("data-subject=\"vorname") == -1)
            return 1;
		if(str2.indexOf("data-subject=\"vorname") == -1)
            return -1;
        return ((str1 < str2) ? 1 : ((str1 > str2) ? -1 : 0));
	 }
});
