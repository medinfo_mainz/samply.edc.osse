javascript:window.history.forward(1);

var lastActive = 0; //#{session.lastAccessedTime};
var maxtime = 0;
var stringTimeoutOne = "";
var stringTimeoutTwo = "";
var stringTimeoutUnder = "";
var stringTimeoutTwoMin = "";


function maxInitValues(la, mt, tto, ttt, ttm, ttu) {
	lastActive = la;
	maxtime = mt;
	stringTimeoutOne = tto;
	stringTimeoutTwo = ttt;
	stringTimeoutUnder = ttu;
	stringTimeoutTwoMin = ttm;
}

function delayLoad() {
	setTimeout(postLoadInit, 1000);
}

function postLoadInit() {
	timeoutFun();
	//TODO: uncomment this for conflict prevention system
	//addListener();
}

function addListener() {
	$('.maxtest').change(function() {
		alert('Alert in '+this);
		//TODO: semaphore add to lock changes in this patient to others
	});
}

function resetTimer()
{
	lastActive = new Date().getTime();
}

function timeoutFun(){
    var milliseconds = new Date().getTime();
    
   // var maxtime = #{session.maxInactiveInterval} - 30; //inaccuracy between server and JS unfortunately so safety zone of 20 seconds
    var lasttime = Math.floor((milliseconds - lastActive) / 1000);
    var resttime = maxtime - lasttime;

    if(resttime <= 100)
    {
    	timeoutDialog.show();
    }
    
    if(resttime <= 0)
    {
    	// We click the logout link :)
    	//var link = document.getElementById('logoutLink');
    	//link.click();
    	logoutButtonVar.jq.click();
    }
        

    if(document.getElementById('dialog-countdown') != null)
    	document.getElementById('dialog-countdown').innerHTML=stringTimeoutOne+" "+resttime+" "+stringTimeoutTwo;

    var resttimeMin = Math.floor(resttime / 60) + 1;
    if(resttimeMin <= 2)
    	document.getElementById('runsout').innerHTML=stringTimeoutOne+" "+stringTimeoutUnder+" 2 "+stringTimeoutTwoMin+"";
    else
    	document.getElementById('runsout').innerHTML=stringTimeoutOne+" "+resttimeMin+" "+stringTimeoutTwoMin+"";
    
    if(resttime <= 100)
    	setTimeout(timeoutFun, 1000);
    else
	    setTimeout(timeoutFun, 5000); // run every 5 seconds, later every minute
}

window.onload = delayLoad();

